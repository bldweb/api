<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::prefix('v1')->group(function () {

	Route::post('/login', 'Auth\ApiLoginController@login');
	Route::post('/login/refresh', 'Auth\ApiLoginController@refresh');

	Route::middleware('auth:api')->group(function () {
	    
	    Route::post('/logout', 'Auth\ApiLoginController@logout');

	    Route::get('/contratos', 'ContratosController@index');
	  
	});
});

