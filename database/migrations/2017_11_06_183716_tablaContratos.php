<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TablaContratos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contratos', function (Blueprint $table) {

            
            /*comprador*/
            $table->bigInteger('comprador_cuit');
            $table->string('comprador_razonSocial');
            $table->integer('comprador_codigoActividad');

            /*vendedor*/
            $table->bigInteger('vendedor_cuit');
            $table->string('vendedor_razonSocial');
            $table->integer('vendedor_codigoActividad');

            /*corredor*/
            $table->bigInteger('corredor_cuit');
            $table->string('corredor_razonSocial');

            /*Procedencia*/
            $table->integer('procedencia_codigoLocalidad');
            $table->string('procedencia_codigoPostalLocalidad');
            $table->string('procedencia_subcodigoPostalLocalidad');
            $table->integer('procedencia_codigoProvincia');
            
            /*Destino*/
            $table->integer('destino_codigoLocalidad');
            $table->string('destino_codigoPostalLocalidad');
            $table->string('destino_subcodigoPostalLocalidad');
            $table->integer('destino_codigoProvincia');

            /*Producto*/
            $table->integer('producto_codigo');
            $table->integer('producto_extension');
            $table->integer('producto_codigoCosecha');
            $table->integer('producto_codigoCondicionesCalidad');

            /*Generales*/
            $table->integer('codigoBolsa');
            $table->date('fechaContrato');
            $table->string('numeroContratoComprador');
            $table->string('numeroContratoVendedor');
            $table->string('numeroContratoCorredor');
            $table->string('codigoUnidadMedida');
            $table->integer('cantidadDesde');
            $table->integer('cantidadHasta');
            $table->integer('codigoAjuste');
            $table->integer('cantidadCamiones');
            $table->string('codigoMedioTransporte');
            $table->integer('codigoAPrecio');
            $table->integer('codigoTipoContrato');
            $table->integer('codigoTipoOperacion');
            $table->integer('codigoMercaderiaDeOperacionPropia');
            $table->integer('codigoModalidadOperacion');
            $table->integer('codigoEsCompradorFinal');
            $table->decimal('porcentajeMercaderiaParcial', 5, 2);
            $table->date('fechaEntregaDesde');
            $table->date('fechaEntregaHasta');
            $table->integer('codigoCondicionPago');
            $table->integer('codigoOpcionFijacion');
            $table->decimal('precio', 18, 2);
            $table->integer('codigoMoneda');
            $table->date('fechaConvenioPago');
            $table->integer('cantidadFijacionMinima');
            $table->integer('cantidadFijacionMaxima');
            $table->string('codigoUnidadMedidaFijacion');
            $table->integer('codigoFijacionPeriodo');
            $table->date('fechaFijcionDesde');
            $table->date('fechaFijcionHasta');
            $table->integer('codigoRegistracionAFIP');
            $table->text('observaciones');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contratos');
    }
}
