<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class ContratosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $faker = Faker::create();
      foreach (range(1,100) as $index) {
        DB::table('contratos')->insert([


            /*comprador*/
            'comprador_cuit' => $faker->randomNumber(8),
            'comprador_razonSocial' => $faker->name,
            'comprador_codigoActividad' => $faker->numberBetween(1,10000),

            /*vendedor*/
            'vendedor_cuit' => $faker->randomNumber(8),
            'vendedor_razonSocial' => $faker->name,
            'vendedor_codigoActividad' => $faker->numberBetween(1,10000),

            /*corredor*/
            'corredor_cuit' => $faker->randomNumber(8),
            'corredor_razonSocial' => $faker->name,

            /*Procedencia*/
            'procedencia_codigoLocalidad' => $faker->numberBetween(1,10000),
            'procedencia_codigoPostalLocalidad' => $faker->numberBetween(1,10000),
            'procedencia_subcodigoPostalLocalidad' => $faker->randomNumber(2),
            'procedencia_codigoProvincia' => $faker->numberBetween(1,19),
            
            /*Destino*/
            'destino_codigoLocalidad' => $faker->numberBetween(1,10000),
            'destino_codigoPostalLocalidad' => $faker->numberBetween(1,10000),
            'destino_subcodigoPostalLocalidad' => $faker->randomNumber(2),
            'destino_codigoProvincia' => $faker->numberBetween(1,19),

            /*Producto*/
            'producto_codigo' => $faker->numberBetween(1,10),
            'producto_extension' => $faker->numberBetween(1,5),
            'producto_codigoCosecha' => $faker->numberBetween(1,1000),
            'producto_codigoCondicionesCalidad' => $faker->numberBetween(1,5),

            /*Generales*/
            'codigoBolsa' => $faker->numberBetween(1,1000),
            'fechaContrato' => $faker->date('Y-m-d','now'),
            'numeroContratoComprador' => $faker->numberBetween(10000,100000),
            'numeroContratoVendedor' => $faker->numberBetween(10000,100000),
            'numeroContratoCorredor' => $faker->numberBetween(10000,100000),
            'codigoUnidadMedida' => 'kilo',
            'cantidadDesde' => $faker->numberBetween(1000,10000),
            'cantidadHasta' => $faker->numberBetween(10000,1000000),
            'codigoAjuste' => 1,
            'cantidadCamiones' => $faker->numberBetween(1,100),
            'codigoMedioTransporte' => 'C',
            'codigoAPrecio' => $faker->numberBetween(1,10),
            'codigoTipoContrato' => $faker->numberBetween(1,10),
            'codigoTipoOperacion' => $faker->numberBetween(1,10),
            'codigoMercaderiaDeOperacionPropia' => $faker->numberBetween(1,10),
            'codigoModalidadOperacion' => $faker->numberBetween(1,10),
            'codigoEsCompradorFinal' => $faker->numberBetween(1,10),
            'porcentajeMercaderiaParcial' => '97.5',
            'fechaEntregaDesde' => $faker->date('Y-m-d','now'),
            'fechaEntregaHasta' => $faker->date('Y-m-d','now'),
            'codigoCondicionPago' => 1,
            'codigoOpcionFijacion' => $faker->numberBetween(1,10),
            'precio' => $faker->randomFloat(2, 500, 10000),
            'codigoMoneda' => 2,
            'fechaConvenioPago' => $faker->date('Y-m-d','now'),
            'cantidadFijacionMinima' => $faker->numberBetween(1000,100000),
            'cantidadFijacionMaxima' => $faker->numberBetween(1000,100000),
            'codigoUnidadMedidaFijacion' => 't',
            'codigoFijacionPeriodo' => 3,
            'fechaFijcionDesde' => $faker->date('Y-m-d','now'),
            'fechaFijcionHasta' => $faker->date('Y-m-d','now'),
            'codigoRegistracionAFIP' => 1234,
            'observaciones' => $faker->text,
        ]);
      }
    }
}
