<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(ContratosTableSeeder::class);

        DB::table('users')->insert([

            'name' => 'Usuario Prueba',
            'email' => 'usuario@test.com.ar',
            'password' => bcrypt('secret'),
            'remember_token' => str_random(10),
            
        ]);
        
    }
}
