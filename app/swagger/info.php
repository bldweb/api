<?php

/**
 * @SWG\Swagger(
 *	   schemes={"http"},	
 *     host="138.197.76.110",
 *     basePath="/api/v1",
 *     @SWG\Info(
 *         version="1.0.0",
 *         title="Api gix",
 *         description="Documentacion tentativa de un api de la empresa que lo implemente usando el protocolo gix. Puede consultar el detalle del protocolo en  [https://www.bcr.com.ar](https://www.bcr.com.ar/Documentos/varios/Especificaci%C3%B3n%20API%20Protocolo%20GIX.pdf).
 						<hr>
 						Usuario de prueba:<br>
 						***Email***:   usuario@test.com.ar
 						***Password***:   secret<br><br><hr>
 ",
 *         @SWG\Contact(
 *             email="andrescellini@gmail.com"
 *         ),
 *     ),
 * )
 */