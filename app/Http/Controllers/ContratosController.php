<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Contrato;
use App\Infrastructure\GixTrait;
use App\Http\Requests\ContratosRequest;

class ContratosController extends Controller
{
    
    use GixTrait;
    /*
		ContratosRequest hace todas las validaciones en cuanto a parametros

    */

    /**
     * @SWG\Get(
     *     path="/contratos",
     *     summary="Devuelve contratos de corretaje",
     *     tags={"contratos"},
     *     description="Devuelve un json con los contratos que el usuario logeado puede visualizar. ",
     *     operationId="contratos",
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="Authorization",
     *         in="header",
     *         required=true,
     *         type="string",
     *         default="Bearer ",
     *         description="Bearer {token} . Token generado en el endpoint login. "
     *     ),
     *     @SWG\Parameter(
     *         name="numeroContratoComprador",
     *         in="query",
     *         description="***UNIVOCO*** - Nº contrato generado por parte del Comprador",
     *         required=false,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="numeroContratoVendedor",
     *         in="query",
     *         description="***UNIVOCO*** - Nº contrato generado por parte del Vendedor",
     *         required=false,
     *         type="string",
     *     ),
     *     @SWG\Parameter(
     *         name="numeroContratoCorredor",
     *         in="query",
     *         description="***UNIVOCO*** - Nº contrato generado por parte del Corredor",
     *         required=false,
     *         type="string",
     *     ),
     *     @SWG\Parameter(
     *         name="fechaContratoDesde",
     *         in="query",
     *         description="***UNIVOCO*** - FechaDesde de contrato, formato *AAAA-MM-DD*",
     *         format="AAAA-MM-DD",
     *         required=false,
     *         type="string",
     *     ),          
     *     @SWG\Parameter(
     *         name="fechaContratoHasta",
     *         in="query",
     *         description="***UNIVOCO*** - FechaHasta de contrato, formato *AAAA-MM-DD*",
     *         format="AAAA-MM-DD",
     *         required=false,
     *         type="string",
     *     ), 
     *     @SWG\Parameter(
     *         name="comprador.cuit",
     *         in="query",
     *         description="cuit del comprador",
     *         required=false,
     *         type="number",
     *     ),
     *     @SWG\Parameter(
     *         name="vendedor.cuit",
     *         in="query",
     *         description="cuit del vendedor",
     *         required=false,
     *         type="number",
     *     ),     
     *     @SWG\Parameter(
     *         name="corredor.cuit",
     *         in="query",
     *         description="cuit del corredor",
     *         required=false,
     *         type="number",
     *     ),
     *     @SWG\Parameter(
     *         name="producto.codigo",
     *         in="query",
     *         description="codigo del producto segun afip",
     *         required=false,
     *         type="number",
     *     ),          
     *     @SWG\Response(
     *         response=200,
     *         description="éxito",     
     *     ),
     *     @SWG\Response(
     *         response=401,
     *         description="sin autorizacion",     
     *     ),
     *     @SWG\Response(
     *         response="422",
     *         description="parametros de entrada inválidos",
     *     ),
     *     @SWG\Response(
     *         response="500",
     *         description="error inesperado por parte del api",
     *     )
     * )
     */
    public function index(ContratosRequest $request)
    {
        // creo una query nueva para ir modificandola segun filtros.
        $query = Contrato::query();
        
        // aca se podría poner un scope que limite los registros solo a los que puede ver el usuario logeado


        // logica para que a partir de filtros en el request genere el query.
        $query = $this->parseGix($query,'contratos');

        
        // ejecucion del query con su paginación.	
        $contratos = $query->paginate($this->pageSizeGix);

        // formateo segun gix
        return $this->responseGix($contratos);

    }
}
