<?php

namespace App\Http\Controllers\Auth;


use Illuminate\Http\Request;
use App\Infrastructure\LoginProxy;
use App\Infrastructure\Requests\LoginRequest;
use App\Infrastructure\Controller;


class ApiLoginController extends Controller
{    

    private $loginProxy;

    public function __construct(LoginProxy $loginProxy)
    {
        $this->loginProxy = $loginProxy;
    }

     /**
     * @SWG\Post(
     *     path="/login",
     *     summary="Autorización de usuarios",
     *     tags={"login"},
     *     description="Genera el token, que luego va a servir para consultar los demas puntos de api. Una vez generado con usuario y contraseña, se debe *     pasar en el header de cualquier consulta Authorization: Bearer {token}",
     *     operationId="login",
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="email",
     *         in="query",
     *         description="email del usuario",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="password",
     *         in="query",
     *         description="contraseña del usuario",
     *         required=true,
     *         type="string",
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="éxito",     
     *     ),
     *     @SWG\Response(
     *         response=401,
     *         description="credenciales invalidas",     
     *     ),
     *     @SWG\Response(
     *         response="422",
     *         description="parametros de entrada inválidos",
     *     ),
     *     @SWG\Response(
     *         response="500",
     *         description="error inesperado por parte del api",
     *     )
     * )
     */
    public function login(LoginRequest $request)
    {
        $email = $request->get('email');
        $password = $request->get('password');

        $attempt = $this->loginProxy->attemptLogin($email, $password);

        return $this->response($attempt);
    }

    /**
     * @SWG\Post(
     *     path="/login/refresh",
     *     summary="Refresco del token",
     *     tags={"refresh"},
     *     description="Refresca el token a partir de una cookie",
     *     operationId="refresh",
     *     produces={"application/json"},
     *     @SWG\Response(
     *         response=200,
     *         description="éxito",     
     *     ),
     *     @SWG\Response(
     *         response=401,
     *         description="sin autorización",     
     *     ),
     *     @SWG\Response(
     *         response="500",
     *         description="error inesperado por parte del api",
     *     )
     * )
     */
    public function refresh(Request $request)
    {
        return $this->response($this->loginProxy->attemptRefresh());
    }

    /**
     * @SWG\Post(
     *     path="/logout",
     *     summary="Logout de usuarios",
     *     tags={"logout"},
     *     description="Revoca el token y la posibilidad de refrescarlo",
     *     operationId="logout",
     *     produces={"application/json"},
     *     @SWG\Response(
     *         response=200,
     *         description="éxito",     
     *     ),
     *     @SWG\Response(
     *         response=401,
     *         description="sin autorización",     
     *     ),
     *     @SWG\Response(
     *         response="500",
     *         description="error inesperado por parte del api",
     *     )
     * )
     */
    public function logout()
    {
        $this->loginProxy->logout();

        return $this->response(null, 204);
    }
    
}
