<?php

namespace App\Http\Requests;

use App\Infrastructure\Requests\ApiRequest;
use Symfony\Component\HttpKernel\Exception\UnprocessableEntityHttpException;

class ContratosRequest extends ApiRequest
{

	/* los univocos operan sin problemas. cuando detecto no univoco debo tener si o si todos los campos claves,*/

    protected $noUnivocos = [
	    'comprador_cuit' => 'integer',
	    'vendedor_cuit' => 'integer',
	    'corredor_cuit' => 'integer',
	    'producto_codigo' => 'integer',
    ];

    protected $univocos = [
	    'numeroContratoComprador' => 'string',
	    'numeroContratoVendedor' => 'string',
	    'numeroContratoCorredor' => 'string',
    ];

    /* que pasa si defino mas de 1 par de fechas? */
    protected $fechas = [
	    'fechaContratoHasta' => 'date',
        'fechaContratoDesde' => 'date',
    ];


    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return array_merge($this->noUnivocos,$this->fechas,$this->univocos,['page' => 'integer|min:1']);
        
    }

    /* validaciones especificas de gix, si no hay cambios en los request de cada punto esto puede estar en api request asi no repetimos codigo*/

    public function withValidator($validator)
	{
			
		$parametros = request()->all();
	
		/* parametros desconocidos */

		if($desconocidos = array_except($parametros, array_keys($this->rules()))){

			throw new UnprocessableEntityHttpException('parametros desconocidos: ' . json_encode(array_keys($desconocidos)));

		}

		/* si detecto ya un parametro univoco, no valido nada mas */

		if( array_intersect(array_keys($this->univocos), array_keys($parametros))){

			return true;

		}

		/* siempre al menos 1 filtro */

		if(!array_except($parametros, ['page'])){

			throw new UnprocessableEntityHttpException('debe incluir al menos un parametro');	

		};


		/* valido que las fechas sean de a pares*/

		$fechas = array_intersect(array_keys($this->fechas), array_keys($parametros));

		if( $fechas && count($fechas) != count($this->fechas) ){

			throw new UnprocessableEntityHttpException('los parametros de fechas claves deben venir siempre de a pares');	

		}
	
		/* validaciones no univocos con fechas requeridas*/

		$noUnivocos = array_intersect(array_keys($this->noUnivocos), array_keys($parametros));

		if(	$noUnivocos && count(array_intersect(array_keys($this->fechas), array_keys($parametros) )) != count($this->fechas) ){

			throw new UnprocessableEntityHttpException('parametros no univocos sin campos de fechas requeridos ' . json_encode($noUnivocos));

		};

	}
}
