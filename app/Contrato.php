<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contrato extends Model
{
    protected $table = 'contratos';

    protected $appends = ['comprador','vendedor','corredor','procedencia','destino','producto'];

    protected $hidden = ['comprador_cuit','comprador_razonSocial','comprador_codigoActividad','vendedor_cuit','vendedor_razonSocial','vendedor_codigoActividad','corredor_cuit','corredor_razonSocial','procedencia_codigoLocalidad','procedencia_codigoPostalLocalidad','procedencia_subcodigoPostalLocalidad','procedencia_codigoProvincia','destino_codigoLocalidad','destino_codigoPostalLocalidad','destino_subcodigoPostalLocalidad','destino_codigoProvincia','producto_codigo','producto_extension','producto_codigoCosecha','producto_codigoCondicionesCalidad'
    ];

    public function getCompradorAttribute(){

        return  [
                   'cuit' => $this->comprador_cuit,
                   'razonSocial' => $this->comprador_razonSocial,
                   'codigoActividad' => $this->comprador_codigoActividad 
                ];
    }

    public function getVendedorAttribute(){

        return  [
                   'cuit' => $this->vendedor_cuit,
                   'razonSocial' => $this->vendedor_razonSocial,
                   'codigoActividad' => $this->vendedor_codigoActividad 
                ];
    }

    public function getCorredorAttribute(){

        return  [
                   'cuit' => $this->corredor_cuit,
                   'razonSocial' => $this->corredor_razonSocial
                ];
    }

    public function getProcedenciaAttribute(){

        return  [
                   'codigoLocalidad' => $this->procedencia_codigoLocalidad,
                   'codigoPostalLocalidad' => $this->procedencia_codigoPostalLocalidad,
                   'subcodigoPostalLocalidad' => $this->procedencia_subcodigoPostalLocalidad,
                   'codigoProvincia' => $this->procedencia_codigoProvincia  
                ];
    }

    public function getDestinoAttribute(){

        return  [
                   'codigoLocalidad' => $this->destino_codigoLocalidad,
                   'codigoPostalLocalidad' => $this->destino_codigoPostalLocalidad,
                   'subcodigoPostalLocalidad' => $this->destino_subcodigoPostalLocalidad,
                   'codigoProvincia' => $this->destino_codigoProvincia  
                ];
    }

    public function getProductoAttribute(){

        return  [
                   'codigo' => $this->producto_codigo,
                   'extension' => $this->producto_extension,
                   'codigoCosecha' => $this->producto_codigoCosecha,
                   'codigoCondicionesCalidad' => $this->producto_codigoCondicionesCalidad  
                ];
    }
}


                