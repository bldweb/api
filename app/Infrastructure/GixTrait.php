<?php

namespace App\Infrastructure;

use JsonSerializable;
use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Http\JsonResponse;


trait GixTrait
{

    protected $pageSizeGix = 20;

    protected function responseGix($data, $statusCode = 200, array $headers = [])
    {
        
        $data = [
            "status" => [
                "code" => 200,
                "mensaje" => "OK",
                "detalle" => "",
            ],
            "metadata" => [
                "pageNumber" => $data->currentPage(),
                "pageSize" => $this->pageSizeGix,
                "totalPages" => $data->lastPage(),
                "nextPageLink" => $data->nextPageUrl(),
                "lastPageLink" => $data->url($data->lastPage()),
            ],
            "data" => $data->items()
        ];

        if ($data instanceof Arrayable && !$data instanceof JsonSerializable) {
            $data = $data->toArray();
        }

        return new JsonResponse($data, $statusCode, $headers);
    }


    protected function parseGix($query,$endpoint)
    {
        
        $request = request();

        $parametroDesconocido = 0;

        /*filtros definidos para contratos*/

        if($endpoint === 'contratos'){

            foreach ($request->all() as $key => $value) {
                    
                switch ($key) {

                    case 'comprador.cuit':
                        $query->where('comprador_cuit', $value);
                        break;
                    case 'vendedor.cuit':
                        $query->where('vendedor_cuit', $value);
                        break;
                    case 'corredor.cuit':
                        $query->where('corredor_cuit', $value);
                        break;
                    case 'producto.codigo':
                        $query->where('producto_codigo', $value);
                        break;
                    case 'fechaContratoDesde':
                        $query->whereDate('fechaContrato', '>=', $value);
                        break;
                    case 'fechaContratoHasta':
                        $query->whereDate('fechaContrato', '<=', $value);
                        break;
                    case 'numeroContratoComprador':
                        $query->where('numeroContratoComprador', $value);
                        break;
                    case 'numeroContratoVendedor':
                        $query->where('numeroContratoVendedor', $value);
                        break;
                    case 'numeroContratoCorredor':
                        $query->where('numeroContratoCorredor', $value);
                        break;

                }        

            }
        }

     
        return $query;
    }


}