<?php

namespace App\Infrastructure\Exceptions;

use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class BadRequestHttpException extends BadRequestHttpException
{
    public function __construct($message = null, \Exception $previous = null, $code = 0)
    {
        parent::__construct('', $message, $previous, $code);
    }
}