<?php

namespace App\Infrastructure;

use Illuminate\Foundation\Validation\ValidatesRequests;
use Optimus\Bruno\LaravelController;

abstract class Controller extends LaravelController
{
}
