#Gix Backend

Boilerplate en php con laravel implementando uno de los endpoints del protocolo.

##Como usarlo

http://138.197.76.110/api/documentation

##Como instalarlo

1. Clonar el repositorio en la carpeta del servidor web correspondiente ( www, htdocs, etc )
2. composer install para instalar vendors del proyecto
3. duplicar el archivo .env.example y renombrarlo a .env , configurar ahi los datos de la base de datos
4. `php artisan migrate --seed` para crear todas las tablas y agregar datos de test. `usuario: usuario@test.com.ar password: secret`
5. `php artisan key:generate` para generar una key que sirva de semilla para encriptacion y tokens
6. `php artisan passport:install` para instalar grants para oAuth, guardar esos valores en el archivo .env

##TODO

- conectar a la base de la bolsa, hacer los demas controladores ( por ahora solo existe contratos )
- generar endpoints que faltan y ver que q clases podemos extraer codigo para no duplicar.
